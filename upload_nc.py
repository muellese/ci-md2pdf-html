"""Upload 'public' folder as 'documentation' to nextcloud."""
import os
import owncloud

folder_token = os.getenv("NC_upload_folder_token")
folder_password = os.getenv("NC_upload_folder_password")

if not folder_token:
    raise ValueError("No 'NC_upload_folder_token' env-var found!")

if not folder_password:
    raise ValueError("No 'NC_upload_folder_password' env-var found!")

oc = owncloud.Client("https://nc.ufz.de")
oc.anon_login(folder_token, folder_password)

print("Removing current documentation...")
try:
    oc.delete("documentation")
except owncloud.HTTPResponseError:
    print("   ...no documentation present")

print("Uploading new documentation...")
os.rename("public", "documentation")
oc.put_directory("", "documentation")
os.rename("documentation", "public")
oc.delete("documentation/index.html")
