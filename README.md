# CI: md2pdf + html

A simple script to convert markdown files to PDFs with a minimal html tree
view.

## .gitlab-ci.yml

If you want to use this script, just add this `.gitlab-ci.yml` file to your
repository. It will automatically create the PDFs and the minimal html page
and deploy it to your gitlab pages site.

You still need to activate a gitlab runner of EVE that can access
`/global/apps/landtech/md_convert`. Also you need to activate the gitlab
pages support in your repository.

```yml
stages:
  - build
  - deploy

pdf:
  stage: build
  script:
    - module load foss/2019b
    - module load texlive/2019
    - module load Graphviz/2.42.2
    - module load Anaconda3
    - module load git-lfs
    - git lfs install
    - git lfs pull
    - source activate /global/apps/landtech/md_convert
    - export MERMAID_FILTER_FORMAT="pdf"
    - git clone https://git.ufz.de/muellese/ci-md2pdf-html.git
    - python ci-md2pdf-html/convert_md.py
    - cd public/
    - tree . -H . -I index.html -o index.html -T "Documentation - PDF list"
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - public
  when: always

nextcloud:
  only:
    - master
  stage: deploy
  dependencies:
    - pdf
  script:
    - module load foss/2019b
    - module load texlive/2019
    - module load Graphviz/2.42.2
    - module load Anaconda3
    - source activate /global/apps/landtech/md_convert
    - git clone https://git.ufz.de/muellese/ci-md2pdf-html.git
    - python ci-md2pdf-html/upload_nc.py
  when: always

pages:
  only:
    - master
  stage: deploy
  dependencies:
    - pdf
  script:
    - echo -e "Deploying..."
  artifacts:
    name: "$CI_COMMIT_REF_NAME"
    paths:
      - public
  when: always
```

## conda env

The used conda environment was created with:

```bash
ml foss/2019b
ml texlive/2019
ml Anaconda3
conda create --prefix your_env_path
source activate your_env_path
conda install -y -c conda-forge nodejs
conda install -y -c conda-forge pandoc
conda install -y -c conda-forge tree
conda install -y -c conda-forge pygments
conda install -y -c conda-forge six
conda install -y -c conda-forge requests
conda install -y -c conda-forge librsvg
pip install pyocclient
npm i -g mermaid-filter
```
