"""Convert markdown to PDF."""
import os
import shutil
import subprocess


# pandoc file.md -o file.pdf
options = []
# options += ["-f", "markdown-implicit_figures"]
# options += ["-t", "pdf"]
# options += ["--number-sections"]
options += ["--toc"]
options += ["-V", "toc-title:Contents"]
options += ["-V", "papersize:a4"]
options += ["-V", "geometry:top=2cm, bottom=1.5cm, left=2cm, right=2cm"]
options += ["--highlight-style", "breezedark"]
options += ["--filter", "mermaid-filter"]

repo = os.path.abspath(os.path.join(os.path.dirname(__file__), ".."))
public, CI = [os.path.join(repo, pth) for pth in ["public", "ci-md2pdf-html"]]
skip = [public, CI, os.path.join(repo, ".git")]
if os.path.isdir(public):
    shutil.rmtree(public)
# files to be converted
md_files = []
pdf_files = []
# copy existing pdfs
pdf_from = []
pdf_to = []

# add absolute path for lua scripts to pandoc options
options += ["--lua-filter", os.path.join(CI, "math.lua")]
options += ["--lua-filter", os.path.join(CI, "linebreaks.lua")]
puppeteer_CI = os.path.join(CI, ".puppeteer.json")

for dirpath, dirnames, filenames in os.walk(repo):
    if any([os.path.commonprefix([dirpath, s]) == s for s in skip]):
        continue
    public_dir = os.path.join(public, os.path.relpath(dirpath, repo))
    os.makedirs(public_dir, exist_ok=True)
    for file in filenames:
        name, ext = os.path.splitext(file)
        if ext == ".md":
            md_files.append(os.path.join(dirpath, file))
            pdf_files.append(os.path.join(public_dir, name + ".pdf"))
        if ext == ".pdf":
            pdf_from.append(os.path.join(dirpath, file))
            pdf_to.append(os.path.join(public_dir, file))

print("Converting MD-Files:")
for md, pdf in zip(md_files, pdf_files):
    print(" ", os.path.relpath(md, repo))
    # use dir of md file as working-dir
    cwd = os.path.dirname(md)
    puppeteer_CWD = os.path.join(cwd, ".puppeteer.json")
    if not os.path.isfile(puppeteer_CWD):
        shutil.copy2(puppeteer_CI, puppeteer_CWD)
    call = ["pandoc", md, "-o", pdf] + options
    subprocess.run(call, cwd=cwd, check=True)

print("Copying PDF-Files:")
for pdf_f, pdf_t in zip(pdf_from, pdf_to):
    print(" ", os.path.relpath(pdf_f, repo))
    shutil.copy2(pdf_f, pdf_t)
